package com.oddle.timeoption.model;

import com.google.common.collect.Lists;

import java.util.List;

public class RestaurantShift {

    List<Shift> shifts = Lists.newArrayList();

    public List<Shift> getShifts() {
        return shifts;
    }

    public void setShifts(List<Shift> shifts) {
        this.shifts = shifts;
    }
}
