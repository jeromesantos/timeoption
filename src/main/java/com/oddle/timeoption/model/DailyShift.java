package com.oddle.timeoption.model;

import com.google.common.collect.Lists;

import java.util.List;

public class DailyShift {

    private String day;

    private List<Interval> intervals = Lists.newArrayList();

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public List<Interval> getIntervals() {
        return intervals;
    }

    public void setIntervals(List<Interval> intervals) {
        this.intervals = intervals;
    }
}
