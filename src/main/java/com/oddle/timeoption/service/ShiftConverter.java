package com.oddle.timeoption.service;

import com.google.common.collect.Lists;
import com.oddle.timeoption.model.DailyShift;
import com.oddle.timeoption.model.RestaurantShift;
import com.oddle.timeoption.model.Shift;
import com.oddle.timeoption.service.util.DailyShiftMapper;
import com.oddle.timeoption.service.util.DayRangeUtil;
import com.oddle.timeoption.service.util.IntervalUtil;
import org.joda.time.Interval;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class ShiftConverter {

    public List<DailyShift> execute(RestaurantShift restaurantShift) {
        Map<String, List<Interval>> operationMap = new HashMap<>();
        for (Shift shift : restaurantShift.getShifts()) {
            if (shift.getEndDay() != null) {
                for (String day : DayRangeUtil.get(shift.getStartDay(), shift.getEndDay())) {
                    if (operationMap.containsKey(day)) {
                        operationMap.get(day).add(new Interval(shift.getStartTime(), shift.getEndTime()));
                    } else {
                        operationMap.put(day, Lists.newArrayList(new Interval(shift.getStartTime(), shift.getEndTime())));
                    }
                }
            } else {
                if (operationMap.containsKey(shift.getStartDay())) {
                    operationMap.get(shift.getStartDay()).add(new Interval(shift.getStartTime(), shift.getEndTime()));
                } else {
                    operationMap.put(shift.getStartDay(), Lists.newArrayList(new Interval(shift.getStartTime(), shift.getEndTime())));
                }
            }
        }

        for (Map.Entry<String, List<Interval>> entry : operationMap.entrySet()) {
            entry.setValue(IntervalUtil.optimizeList(entry.getValue()));
        }

        return DailyShiftMapper.map(operationMap);
    }

}
