package com.oddle.timeoption.service.util;

import com.google.common.collect.Lists;
import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.springframework.util.CollectionUtils;

import java.util.List;

public class IntervalUtil {

    public static List<Interval> optimizeList(List<Interval> oldIntervalList) {
        List<Interval> updatedList = Lists.newArrayList();

        for (Interval interval : oldIntervalList) {
            if (CollectionUtils.isEmpty(updatedList)) {
                updatedList.add(interval);
            } else {
                Boolean hasOverlapped = true;
                Boolean isAdded = false;

                while (hasOverlapped) {
                    hasOverlapped = false;

                    List<Interval> copyList = Lists.newArrayList(updatedList);
                    copyList.remove(interval);
                    for (Interval updatedInterval : copyList) {
                        if (updatedInterval.overlaps(interval) || matchEndAndStartInterval(updatedInterval, interval)) {
                            updatedList.remove(updatedInterval);
                            updatedList.remove(interval);

                            interval = unionInterval(interval, updatedInterval);
                            updatedList.add(interval);
                            hasOverlapped = true;
                            isAdded = true;
                            break;
                        }
                    }

                }

                if (!isAdded) {
                    updatedList.add(interval);
                }
            }
        }

        return updatedList;
    }

    private static Interval unionInterval(Interval firstInterval, Interval secondInterval) {
        DateTime start =  firstInterval.getStart().isBefore( secondInterval.getStart() )  ? firstInterval.getStart() : secondInterval.getStart();
        DateTime end =  firstInterval.getEnd().isAfter( secondInterval.getEnd() )  ? firstInterval.getEnd() : secondInterval.getEnd();

        return new Interval(start, end);
    }

    private static Boolean matchEndAndStartInterval(Interval firstInterval, Interval secondInterval) {
        return firstInterval.getStart().equals(secondInterval.getEnd()) || firstInterval.getEnd().equals(secondInterval.getStart());
    }

}
