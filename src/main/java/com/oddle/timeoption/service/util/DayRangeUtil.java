package com.oddle.timeoption.service.util;


import com.google.common.collect.Lists;

import java.util.LinkedList;
import java.util.List;

public class DayRangeUtil {

    public final static List<String> DAYS = new LinkedList<>();

    static {
        DAYS.add("SUNDAY");
        DAYS.add("MONDAY");
        DAYS.add("TUESDAY");
        DAYS.add("WEDNESDAY");
        DAYS.add("THURSDAY");
        DAYS.add("FRIDAY");
        DAYS.add("SATURDAY");

    }

    public static List<String> get(String firstDay, String lastDay) {
        List<String> days = Lists.newLinkedList();

        Integer firstDayIndex =  DAYS.indexOf(firstDay.toUpperCase());
        Integer lastDayIndex = DAYS.indexOf(lastDay.toUpperCase());

        if (firstDayIndex < lastDayIndex) {
            days.addAll(DAYS.subList(firstDayIndex, lastDayIndex+1));
        } else {
            days.addAll(DAYS.subList(firstDayIndex, 7));
            days.addAll(DAYS.subList(0, lastDayIndex + 1));
        }

        return days;
    }

}
