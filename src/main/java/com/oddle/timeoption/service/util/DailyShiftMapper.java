package com.oddle.timeoption.service.util;


import com.google.common.collect.Lists;
import com.oddle.timeoption.model.DailyShift;
import org.joda.time.Interval;

import java.util.List;
import java.util.Map;

public class DailyShiftMapper {

    public static List<DailyShift> map(Map<String, List<Interval>> operatingHoursMap) {
        List<DailyShift> shifts = Lists.newArrayList();

        for (String day : DayRangeUtil.DAYS) {
            DailyShift dailyShift = new DailyShift();
            dailyShift.setDay(day);

            if (operatingHoursMap.containsKey(day)) {
                for (Interval iv : operatingHoursMap.get(day)) {
                    com.oddle.timeoption.model.Interval interval = new com.oddle.timeoption.model.Interval();
                    interval.setStartTime(iv.getStart());
                    interval.setEndTime(iv.getEnd());
                    dailyShift.getIntervals().add(interval);
                }
            }
            shifts.add(dailyShift);
        }

        return shifts;
    }
}
