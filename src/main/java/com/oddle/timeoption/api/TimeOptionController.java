package com.oddle.timeoption.api;

import com.oddle.timeoption.model.DailyShift;
import com.oddle.timeoption.model.RestaurantShift;
import com.oddle.timeoption.service.ShiftConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/timeoption")
public class TimeOptionController {

    @Autowired
    private ShiftConverter shiftConverter;

    @RequestMapping(value ="/convert", method = RequestMethod.POST)
    public ResponseEntity<List<DailyShift>> getOperatingTime(@RequestBody RestaurantShift restaurantShift) {
        return new ResponseEntity<>(shiftConverter.execute(restaurantShift), HttpStatus.OK);
    }
}
