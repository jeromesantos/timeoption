(function () {
  'use strict';

  angular
    .module('timeoptionApp')
    .controller('TimeOptionController', TimeOptionController);

    TimeOptionController.$inject = ['TimeOptionService'];

    function TimeOptionController(TimeOptionService) {
      var vm = this;
      vm.shifts = [];
      vm.days = ['SUNDAY', 'MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY'];
      vm.dailyOperations = [];

      vm.beforeRender = function ($upDate, $leftDate, $rightDate) {
          $leftDate.selectable = false;
          $rightDate.selectable = false;
          $upDate.selectable = false;
      }

      vm.addShift = addShift;
      vm.removeShift = removeShift;
      vm.submit = submit;


      function addShift() {
        console.log(vm.shift);
        vm.shifts.push(vm.shift);
        vm.shift = {};
      }

      function removeShift(index) {
        vm.shifts.splice(index, 1);
      }

      function submit() {
        TimeOptionService.convert({shifts: vm.shifts}).then(function (result) {
          vm.dailyOperations = result.data;
          console.log(vm.dailyOperations);
        });
      }



    }

})();
