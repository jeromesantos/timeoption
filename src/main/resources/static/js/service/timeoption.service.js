(function () {
  'use strict';

  angular.module('timeoptionApp')
    .factory('TimeOptionService', TimeOptionService);

  TimeOptionService.$inject = ['$http'];
  function TimeOptionService($http, $q) {
    return {
      convert: convert

    };

    function convert(shift) {
      console.log("shift", shift);
      return $http.post('api/timeoption/convert', shift);
    }

  }
})();
